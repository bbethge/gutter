# Gutter
This is a project I started for no apparent reason to make a fast, monolithic desktop panel that looks good on the right edge of the screen.

**Caution**: Since I never really used it exclusively, there is a quit button that terminates the panel (although you might think it would log you out).  So, don’t click it if you are trying to use this as your exclusive panel. ☺

At the time I thought making it monolithic (i.e., no rearrangeable applets) would make it fast, but now I’m not so sure.  My main motivation was thinking that we should not have tabs inside things like text editors because the window switcher is already designed to help us switch between documents.  I also thought we shouldn’t have multiple desktops for some reason (paradoxically, it may have been because I was still using browser tabs and the need to use the web browser for multiple activities kept pulling me back to the same desktop).  Thus, I needed space for a lot of window buttons in the switcher, which can be achieved with a vertical panel.  The default panels for [XFCE](https://xfce.org/) and Cinnamon don’t support this very well, so I made my own.

So far the main distinctive feature is that the window switcher buttons share all the vertical space (to make them as easy as possible to click) and they pop out to show you the full title when you hover over them.

## Compiling
This project uses GNU Autotools.  If you check the code out from Git, you will have to run

    $ aclocal
    $ autoconf
    $ automake --add-missing --foreign

before you can run `./configure && make`.  If you want to make a distribution tarball that requires neither Autotools nor Vala to build, you can run `make dist`.

Since I now use Cinnamon, this program uses cinnamon-menus to load the application menu instead of garcon.  You need to have the file `CMenu-3.0.gir` installed in `$GIRDIR`, which defaults to `/usr/share/gir-1.0`.  Hopefully this file is included in your cinnamon-menus package as it is on Manjaro.
