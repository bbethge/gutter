namespace Gutter {

public delegate void XEventFilter(X.Event event);

public class XEventFilterManager {
    public int type;
    public unowned XEventFilter filter;
    
    static Gee.MultiMap<X.Window?, unowned XEventFilterManager>
        xwindow_to_xefms =
        new Gee.HashMultiMap<X.Window?, unowned XEventFilterManager> (
            (a) => {
                var aa = (!)(X.Window?)a;
                return (uint) (((aa>>32)^aa) & 0xfffffffful);
            },
            (a, b) => { return (X.Window?)a == (X.Window?)b; }
        );
    
    protected static Gdk.FilterReturn wrapper(
        Gdk.XEvent gdk_xev, Gdk.Event ev
    ) {
        // Caution: Gdk.XEvent is a class, so it is passed by
        // reference.  But X.Event is a struct, so it is passed by
        // value and casting Gdk.XEvent to X.Event won’t work.  It has
        // to be cast to a pointer.
        var xev = (X.Event*)gdk_xev;
        foreach (var xefm in xwindow_to_xefms[xev.xany.window]) {
            if (xev.type == xefm.type) {
                xefm.filter(*xev);
            }
        }
        if (xev.type == X.EventType.DestroyNotify) {
            xwindow_to_xefms.remove_all(xev.xany.window);
            var display =
                Gdk.X11.Display.lookup_for_xdisplay(xev.xany.display);
            // TODO: What if display == null?
            var window =
                new Gdk.X11.Window.foreign_for_display(
                    display, xev.xany.window
                );
            window.remove_filter(wrapper);
            // TODO: anything else?
        }
        return Gdk.FilterReturn.CONTINUE;
    }
    
    protected static Quark key = Quark.from_string("gutter_xevent_filter");
    
    public static void add(
        Gdk.Window window, int type, Object owner, XEventFilter filter
    ) {
        if (window.is_destroyed()) return;
        
        // This function may be called before its class is initialized, so it is
        // very important to create an instance (which initializes the class)
        // before using any other class methods or variables.
        var xefm = new XEventFilterManager();
        xefm.type = type;
        xefm.filter = filter;
        
        var xwindow = (window as Gdk.X11.Window).get_xid();
        
        xwindow_to_xefms[xwindow] = xefm;
        window.add_filter(wrapper);
        
        // Give ‘owner’ a reference to ‘xefm’ so that ‘xefm’ will be
        // automatically freed when ‘owner’ is destroyed.
        Gee.List<XEventFilterManager>? xefms =
            owner.get_qdata<Gee.List<XEventFilterManager>> (key);
        if (xefms == null) {
            xefms = new Gee.LinkedList<XEventFilterManager> ();
            owner.set_qdata<Gee.List<XEventFilterManager>> (key, xefms);
        }
        xefms.insert(0, xefm);
    }
}

}  // end namespace Gutter
