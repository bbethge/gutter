namespace Gutter {

public class Menu: Gtk.Button {
    protected GMenu.Tree? menu;
    protected Gtk.Menu appl_menu;
    
    construct {
        this.menu = new GMenu.Tree(
            "cinnamon-applications.menu", GMenu.TreeFlags.NONE
        );
        try {
            this.menu.load_sync();
        }
        catch (Error e) {
            warning(_("Error while loading applications menu: %s"), e.message);
            this.menu = null;
        }
        
        var hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        hbox.show();
        this.add(hbox);
        
        var image = new Gtk.Image.from_icon_name(
            "applications-all", Gtk.IconSize.LARGE_TOOLBAR
        );
        image.show();
        hbox.pack_start(image, false, false, 0);
        
        var label = new Gtk.Label(_("Applications"));
        label.xalign = 0.0f;
        label.show();
        hbox.pack_start(label, true, true, 0);
        
        this.appl_menu = new Gtk.Menu();
        if (this.menu != null) {
            build_menu(this.appl_menu, this.menu.get_root_directory());
        }
        
        var sep = new Gtk.SeparatorMenuItem();
        this.appl_menu.append(sep);
        sep.show();
        
        var log_out_item = build_menu_item(
            new GLib.ThemedIcon("system-log-out"), _("Log Out")
        );
        log_out_item.activate.connect((menu_item) => {
            try {
                GLib.Process.spawn_async(
                    GLib.Environment.get_home_dir(),
                    { "cinnamon-session-quit", "--logout" },
                    null, GLib.SpawnFlags.SEARCH_PATH, null, null
                );
            }
            catch (GLib.SpawnError e) {
                warning("Could not launch logout dialog: %s", e.message);
            }
        });
        log_out_item.show();
        this.appl_menu.append(log_out_item);
        
        var power_off_item = build_menu_item(
            new GLib.ThemedIcon("system-shutdown"), _("Power Off")
        );
        power_off_item.activate.connect((menu_item) => {
            try {
                GLib.Process.spawn_async(
                    GLib.Environment.get_home_dir(),
                    { "cinnamon-session-quit", "--power-off" },
                    null, GLib.SpawnFlags.SEARCH_PATH, null, null
                );
            }
            catch (GLib.SpawnError e) {
                warning("Could not launch power-off dialog: %s", e.message);
            }
        });
        power_off_item.show();
        this.appl_menu.append(power_off_item);
    }
    
    protected static void build_menu(Gtk.Menu menu, GMenu.TreeDirectory dir) {
        var iter = dir.iter();
        while (true) {
            var done = false;
            Gtk.MenuItem? item = null;
            switch (iter.next()) {
                case GMenu.TreeItemType.INVALID:
                    done = true;
                    break;
                case GMenu.TreeItemType.DIRECTORY:
                    item = build_directory(iter.get_directory());
                    break;
                case GMenu.TreeItemType.ENTRY:
                    item = build_entry(iter.get_entry());
                    break;
                case GMenu.TreeItemType.SEPARATOR:
                    item = new Gtk.SeparatorMenuItem();
                    break;
                case GMenu.TreeItemType.HEADER:
                    // TODO: Not sure what to do here
                    break;
                case GMenu.TreeItemType.ALIAS:
                    var alias = iter.get_alias();
                    switch (alias.get_aliased_item_type()) {
                        case GMenu.TreeItemType.DIRECTORY:
                            item = build_directory(
                                alias.get_aliased_directory()
                            );
                            break;
                        case GMenu.TreeItemType.ENTRY:
                            item = build_entry(alias.get_aliased_entry());
                            break;
                        default:
                            warning(
                                "GMenu.TreeAlias is not a directory or entry"
                            );
                            break;
                    }
                    break;
            }
            if (done) {
                break;
            }
            if (item != null) {
                item.show();
                menu.append(item);
            }
        }
    }
    
    protected static Gtk.MenuItem build_directory(GMenu.TreeDirectory dir) {
        if (dir.get_icon() == null) {
            warning("directory icon is null");
        }
        var item = build_menu_item(dir.get_icon(), dir.get_name());
        var submenu = new Gtk.Menu();
        build_menu(submenu, dir);
        item.set_submenu(submenu);
        return item;
    }
    
    protected static Gtk.MenuItem build_entry(GMenu.TreeEntry entry) {
        var app_info = entry.get_app_info();
        var item = build_menu_item(
            app_info.get_icon(), entry.get_app_info().get_name()
        );
        item.activate.connect((mi) => { on_menu_item_activate(mi, entry); });
        return item;
    }
    
    protected static Gtk.MenuItem build_menu_item(GLib.Icon? icon, string name)
    {
        var item = new Gtk.MenuItem();
        Gtk.Image image;
        if (icon != null) {
            image = new Gtk.Image.from_gicon(icon, Gtk.IconSize.MENU);
        }
        else {
            var def_icon = new GLib.ThemedIcon("application-default-icon");
            image = new Gtk.Image.from_gicon(def_icon, Gtk.IconSize.MENU);
        }
        var hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        hbox.add(image);
        hbox.add(new Gtk.Label(name));
        hbox.show_all();
        item.add(hbox);
        return item;
    }
    
    public override void pressed() {
        base.pressed();
        this.appl_menu.popup_at_widget(
            this, Gdk.Gravity.NORTH_WEST, Gdk.Gravity.NORTH_EAST, null
        );
    }
    
    static void on_menu_item_activate(
        Gtk.MenuItem item, GMenu.TreeEntry tree_entry
    ) {
        var app_info = tree_entry.get_app_info();
        try {
            var context = item.get_display().get_app_launch_context();
            app_info.launch(null, context);
        }
        catch (Error err) {
            // TODO: Graphical error message
            //warning("Could not launch %s: %s", item.name ?? "", err.message);
            return;
        }
    }
}

}  // end namespace Gutter
